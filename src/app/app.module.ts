import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

import { MyApp } from './app.component';
import { NewsFeedPage } from '../pages/newsFeed/newsFeed';
import { ContactPage } from '../pages/contact/contact';
import { CouponListPage } from '../pages/couponList/couponList';
import { CouponSinglePage } from '../pages/couponSingle/couponSingle';
import { TabsPage } from '../pages/tabs/tabs';
import { DbService } from '../services/db.service'

export const firebaseConfig = {
  apiKey: 'AIzaSyC8ltx3KdlLjEePMjeXZQL384CmTJbY9_4',
  authDomain: 'coupon-starter.firebaseapp.com',
  databaseURL: 'https://coupon-starter.firebaseio.com',
  storageBucket: 'coupon-starter.appspot.com'
}

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    NewsFeedPage,
    ContactPage,
    CouponListPage,
    CouponSinglePage,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    NewsFeedPage,
    ContactPage,
    CouponListPage,
    CouponSinglePage,
    TabsPage
  ],
  providers: [DbService]
})
export class AppModule {}
