import { Component } from '@angular/core'
import { NavController } from 'ionic-angular'

import { CouponSinglePage } from '../couponSingle/couponSingle'
import { DbService } from '../../services/db.service'

@Component({
  selector: 'page-couponList',
  templateUrl: 'couponList.html'
})
export class CouponListPage {
  coupons

  constructor(public navCtrl: NavController, public dbService: DbService) {
    this.getCoupons()
  }

  getCoupons() {
    this.coupons = this.dbService.getCoupons()
  }

  useThisCouponButton(coupon) {
    this.navCtrl.push(CouponSinglePage, { coupon: coupon })
  }
}
