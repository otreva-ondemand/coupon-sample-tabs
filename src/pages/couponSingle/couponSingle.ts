import { Component } from '@angular/core'
import { NavController, NavParams } from 'ionic-angular'

@Component({
  selector: 'page-couponSingle',
  templateUrl: 'couponSingle.html'
})
export class CouponSinglePage {
  coupon

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.coupon = navParams.get('coupon')
  }

  backButton() {
    this.navCtrl.pop()
  }
}
