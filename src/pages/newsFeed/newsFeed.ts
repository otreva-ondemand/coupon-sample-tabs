import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-newsFeed',
  templateUrl: 'newsFeed.html'
})
export class NewsFeedPage {

  constructor(public navCtrl: NavController) {

  }

}
