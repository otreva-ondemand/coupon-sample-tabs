import { Component } from '@angular/core';

import { CouponListPage } from '../couponList/couponList';
import { NewsFeedPage } from '../newsFeed/newsFeed';
import { ContactPage } from '../contact/contact';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = NewsFeedPage;
  tab2Root: any = CouponListPage;
  tab3Root: any = ContactPage;

  constructor() {

  }
}
