import { Injectable } from '@angular/core'
import { AngularFire } from 'angularfire2'

@Injectable()
export class DbService {
  couponsList

  constructor(public af: AngularFire) {
    this.couponsList = this.af.database.list('/coupons')
  }

  getCoupons() {
    return this.couponsList
  }
}

/*
class MyComp {
  questions: FirebaseListObservable<any[]>
  value: FirebaseObjectObservable<any>
  constructor(af: AngularFire) {
    this.questions = af.database.list('/questions')
    this.value = af.database.object('/value')
  }
  addToList(item: any) {
    this.questions.push(item)
  }
  removeItemFromList(key: string) {
    this.questions.remove(key).then(_ => console.log('item deleted!'))
  }
  deleteEntireList() {
    this.questions.remove().then(_ => console.log('deleted!'))
  }
  setValue(data: any) {
    this.value.set(data).then(_ => console.log('set!'))
  }
  updateValue(data: any) {
    this.value.update(data).then(_ => console.log('update!'))
  }
  deleteValue() {
    this.value.remove().then(_ => console.log('deleted!'))
  }
}
*/
